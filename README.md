# manjaro-settings



## Getting started

### Configure the keyboard correctly for abnt2:

~~~bash
localectl set-keymap br-abnt2
localectl set-x11-keymap br abnt2 
localectl status  # just to check the actual config
~~~

After that, reboot the machine. If it doesn't work try again with root user


### Adjusting the repo mirrors:

Installing with default options just configured the `Brazil` mirror, that doesn't work very well.

~~~bash
# Configuring mirrors fom my continent
sudo pacman-mirrors --continent && sudo pacman -Syyu
~~~

More detailed instructions about pacman mirrors can be found at https://wiki.manjaro.org/index.php/Pacman-mirrors


### Configuring the default sound output:

~~~bash
# Check the pulseaudio service
systemctl --user status pulseaudio

# Check the available sound outputs
pacmd list-sinks
~~~

To make the pulseaudio service start with correct default sound output, add this to the file `/etc/pulse/default.pa`:
~~~bash
# set-default-sink sound-card.profile 
set-default-sink alsa_output.pci-0000_01_00.1.hdmi-stereo-extra1
~~~

In some cases, this may not work because the pulseaudio service can start before the selected hdmi port is ready.

So you have to edit the priority values to increase the priority of the selected option
~~~bash
# Checking audio devices priority values
grep -R priority /usr/share/pulseaudio/alsa-mixer/paths/

# Editing the system default output to decrease priority (59 --> 58)
sudo vim /usr/share/pulseaudio/alsa-mixer/paths/hdmi-output-0.conf

# Editing the desired output to increase priority (58 --> 59)
sudo vim /usr/share/pulseaudio/alsa-mixer/paths/hdmi-output-1.conf
~~~

More detailed instructions about pacman mirrors can be found at https://wiki.manjaro.org/index.php/Pacman-mirrors



